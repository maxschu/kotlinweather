package com.soft.dude.kotlintraining.weatherapp.ui.utils

import android.content.Context
import android.view.View

class ViewExtensions {
    val View.ctx: Context
        get() = context
}