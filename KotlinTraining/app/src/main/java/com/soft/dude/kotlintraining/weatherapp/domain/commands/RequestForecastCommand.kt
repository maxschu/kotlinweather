package com.soft.dude.kotlintraining.weatherapp.domain.commands

import com.soft.dude.kotlintraining.weatherapp.data.ForecastRequest
import com.soft.dude.kotlintraining.weatherapp.domain.mappers.ForecastDataMapper
import com.soft.dude.kotlintraining.weatherapp.domain.model.ForecastList

class RequestForecastCommand(private val zipCode: Long) : Command<ForecastList> {

    override fun execute(): ForecastList {
        val forecastRequest = ForecastRequest(zipCode)
        return ForecastDataMapper().convertFromDataModel(zipCode, forecastRequest.execute())
    }

}