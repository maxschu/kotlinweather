package com.soft.dude.kotlintraining.weatherapp.domain.commands

interface Command<out T> {
    fun execute(): T
}